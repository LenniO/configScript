#!/usr/bin/env bash
set -e

flatpak install -y flathub com.github.tchx84.Flatseal

flatpak install -y flathub org.freecadweb.FreeCAD
sudo flatpak override org.freecadweb.FreeCAD --filesystem=~/.var/app/org.freecadweb.FreeCAD
flatpak info --show-permissions org.freecadweb.FreeCAD
