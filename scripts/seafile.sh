#!/usr/bin/env bash
set -e

sudo wget https://linux-clients.seafile.com/seafile.asc -O /usr/share/keyrings/seafile-keyring.asc
sudo bash -c "echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/seafile-keyring.asc] https://linux-clients.seafile.com/seafile-deb/focal/ stable main' > /etc/apt/sources.list.d/seafile.list"

sudo apt update
sudo apt install -y seafile-gui

cp files/seafile-applet.desktop ~/.config/autostart/
