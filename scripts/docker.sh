#!/usr/bin/env bash
set -e

sudo apt update

packages=(
  docker.io
)

sudo apt install -y ${packages[@]}

sudo groupadd -f docker
sudo usermod -aG docker $USER
