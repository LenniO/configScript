#!/usr/bin/env bash
set -e

INSTALL_DIR=~/bin/jetbrains-toolbox
INSTALL_EXE=${INSTALL_DIR}/jetbrains-toolbox

if [ -f ${INSTALL_EXE} ]; then
    echo "jetbrains-toolbox is already installed"
    exit 0
fi

USER_AGENT=('User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36')

URL=$(curl 'https://data.services.jetbrains.com//products/releases?code=TBA&latest=true&type=release' -H 'Origin: https://www.jetbrains.com' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.8' -H "${USER_AGENT[@]}" -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Referer: https://www.jetbrains.com/toolbox/download/' -H 'Connection: keep-alive' -H 'DNT: 1' --compressed | grep -Po '"linux":.*?[^\\]",' | awk -F ':' '{print $3,":"$4}'| sed 's/[", ]//g')
echo $URL

FILE_NAME=$(basename ${URL})
FILE_DOWNLOAD_PATH=/tmp/$FILE_NAME

wget -cO  ${FILE_DOWNLOAD_PATH} ${URL} --read-timeout=5 --tries=0
mkdir -p ${INSTALL_DIR}
tar -xzf ${FILE_DOWNLOAD_PATH} -C ${INSTALL_DIR} --strip-components=1

sudo chmod +rwx ${INSTALL_EXE}

${INSTALL_EXE} &disown

#todo: dont add if already there
echo fs.inotify.max_user_watches=32768 | sudo tee /etc/sysctl.d/idea.conf
