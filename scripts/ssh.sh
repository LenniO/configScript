#!/usr/bin/env bash
set -e

mkdir -p ~/.ssh
cp files/sshConfig ~/.ssh/config

#todo: require ~/Seafile/Confidential
chmod 600 ~/Seafile/Confidential/gitlab_id_rsa
