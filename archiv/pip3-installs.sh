#!/usr/bin/env bash
set -e

#pip3 install --user --upgrade pip
pip3 install --user numpy
pip3 install --user opencv-python
pip3 install --user matplotlib
pip3 install --user scipy
pip3 install --user xlrd
#pip3 install --user conan
#Datenvisualisierung:
pip3 install --user seaborn

#mkdir -p ~/.conan/profiles
#cp files/conanProfiles/* ~/.conan/profiles
#todo: use conan config instead
