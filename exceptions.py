
class ProcessReturnedErrorCode(Exception):
    def __init__(self, errorCode, output=""):
        self._errorCode=errorCode
        self._output=output

    @property
    def errorCode(self):
        return self._errorCode

    @property
    def output(self):
        return self._output
