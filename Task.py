
class Task:
    def __init__(self, name, actions):
        self._name = name
        if type(actions) is not list:
            actions = [actions]
        self._actions = actions

    @property
    def name(self):
        return self._name

    def execute(self):
        for action in self._actions:
            action()
